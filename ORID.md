# ORID



## Objective

- I mainly conducted a Code Review of last week's homework, and then I gave a group speech, the topic of which was about design patterns.  The topic of our group speech is command pattern.  Through each group speech, I have a basic understanding of command pattern, strategy pattern and observer pattern.
- In addition, I mainly learned the concepts and methods of code refactoring. Through learning, I understand the importance of code reconstruction and how to carry out code reconstruction, which will be of great help to my future code writing.

## Reflective

- I feel very challenged about design patterns and excited about code refactoring.


## Interpretative

- Although I had a preliminary understanding of the three design patterns through the group speech and listening to the explanation of other groups, I still found the design pattern a very difficult knowledge point, because I did not know when to use it and it was difficult to judge which design pattern to use. Therefore, I found this part of learning very challenging. The reason I was excited was that I learned how to do code refactoring, which helped me a lot in my future programming.

## Decision

- I would continue to learn the basic concepts of design patterns and consider whether I could use them in my daily code writing. In addition, I will also strengthen the ability to refactor the code, and strive to write more elegant and understandable code.