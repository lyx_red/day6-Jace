import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public String getResult(String inputStr) {
        try {
            String[] wordsArray = inputStr.split("\\s+");
            List<Input> inputList = getInputList(wordsArray);
            List<Input> resultInputList = reduceInputList(inputList);
            return getWordFrequencyString(resultInputList);
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private static String getWordFrequencyString(List<Input> inputList) {
        StringJoiner joiner = new StringJoiner("\n");
        inputList.stream().map(word -> word.getWord() + " " + word.getWordCount()).forEach(joiner::add);
        return joiner.toString();
    }

    public List<Input> reduceInputList(List<Input> inputList) {
        Map<String, Integer> inputsMap = getListMap(inputList);
        return inputsMap.entrySet()
                .stream()
                .map(entry -> new Input(entry.getKey(), entry.getValue()))
                .sorted((word1, word2) -> word2.getWordCount() - word1.getWordCount()).collect(Collectors.toList());
    }

    private static List<Input> getInputList(String[] wordsArray) {
        ArrayList<Input> inputList = new ArrayList<>();
        Arrays.stream(wordsArray).forEach(word -> inputList.add(new Input(word, 1)));
        return inputList;
    }

    public Map<String, Integer> getListMap(List<Input> inputList) {
        Map<String, Integer> map = new HashMap<>();
        inputList.forEach(input -> map.put(input.getWord(), map.getOrDefault(input.getWord(), 0) + 1));
        return map;
    }
}
